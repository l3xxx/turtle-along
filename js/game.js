$(function() {
  kilppari = {
    anim: {
      animSpeed: 100,
      delta: 0,
      walk: [-27, -167, -307, -447, -587],
      hide: [-727, -867, -1007],
      show: [-1007, -867, -727],
      current: 'show',
      currentFrame: 0,
      player: $('kilppari'),
      left: 0,
      top: 500,
      speed: 3,
      update: function (delta) {
        this.delta += delta;
        if (this.delta >= this.animSpeed) {
          this.delta = 0;
          this.player.css({
            left: this.left,
            top: this.top
          });
          if (this.current === 'walk') {
            this.left += this.speed;
            this.player.css({
              backgroundPositionY: this[this.current][this.currentFrame++ % 5]
            });
          } else if (this.current === 'hide') {
            if (this.currentFrame < 3) {
              this.player.css({
                backgroundPositionY: this[this.current][this.currentFrame++]
              });
            }
          } else if (this.current === 'show') {
            if (this.currentFrame < 3) {
              this.player.css({
                backgroundPositionY: this[this.current][this.currentFrame++]
              });
            } else {
              this.change('walk');
            }
          }
        }
      },
      change(anim) {
        this.currentFrame = 0;
        this.current = anim;
      }
    }
  };
  bat = {
    anim: {
      animSpeed: 50,
      delta: 0,
      nice: 0,
      evil: 142,
      toEvil: 284,
      toNice: 426,
      width: 142,
      height: 75.5,
      current: 'nice',
      currentFrame: 0,
      bat: $('bat'),
      left: 150,
      top: 200,
      speed: 7,
      changeTo: null,
      rotation: 0,
      rotateSpeed: 2,
      scale: .5,
      directionFrames: 35,
      directionMoved: 0,
      xDirection: 0,
      update: function (delta) {
        this.delta += delta;
        if (this.delta >= this.animSpeed) {
          this.delta = 0;
          this.bat.css({
            backgroundPositionX: -1 * this[this.current],
            backgroundPositionY: (this.currentFrame++) % 9 * -this.height,
            transform: 'rotate(' + this.rotation + 'deg) scale( ' + this.scale + ')',
            left: this.left,
            top: this.top + Math.sin(this.directionMoved/100) * 200
          });

          if (this.directionMoved % this.directionFrames === 0) {
            this.xDirection = this.speed * (kilppari.anim.left > this.left ? 1 : -1);
          }
          this.left += this.xDirection;
          this.directionMoved ++;
          this.rotation += this.xDirection * this.rotateSpeed;
          if (this.rotation > 45) {
            this.rotation = 45;
          }
          if (this.rotation < -45) {
            this.rotation = -45;
          }

          if (this.changeTo && this.currentFrame % 9 === 8) {
            this.change(this.changeTo);
            this.changeTo = null;
          }
          if (this.current === 'evil') {
          }
          if (this.current === 'toEvil') {
            if (this.currentFrame === 8) {
              this.change('evil');
            }
            this.scale += .1;
          } else if (this.current === 'toNice') {
            if (this.currentFrame === 8) {
              this.change('nice');
            }
            this.scale -= .1;
          }
        }
      },
      change(anim) {
        this.currentFrame = 0;
        this.current = anim;
      }
    }
  };

  $(document).keydown(function(event) {
    if (event.key === ' ') {
      if (kilppari.anim.current === 'walk') {
        kilppari.anim.change('hide');
      }
    }
    if (event.key === 'Escape') {
      if (bat.anim.current === 'nice') {
        bat.anim.changeTo = 'toEvil';
      } else if (bat.anim.current === 'evil') {
        bat.anim.changeTo = 'toNice';
      }
    }
  });

  $(document).keyup(function(event) {
    if (event.key === ' ') {
      if (kilppari.anim.current === 'hide') {
        kilppari.anim.change('show');
      }
    }

  });

  MainLoop.setUpdate(update).start();

  paused = false;
  function update(delta) {
    if (paused) {return};
    kilppari.anim.update(delta);
    bat.anim.update(delta);
  }

  pause = function() {
    paused = !paused;
  };

  restart = function() {
    document.location.reload();
  };
});


